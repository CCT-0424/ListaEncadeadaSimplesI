#include <iostream>

using namespace std;

struct num {
    int numero;
    struct num *proximo;
};

struct num *inicio = NULL;
struct num *fim = NULL;

void adicionar(num **inicio, num **fim){
  num *temp = new num;
  cout << "Digite um número: ";
  cin >> temp->numero;
  temp->proximo = NULL;
  if (*inicio == NULL){
    *inicio = temp;
    *fim = temp;
  } else {
    (*fim)->proximo = temp;
    *fim = temp;
  }
}

void listar(num **inicio){
    num *temp;
    temp = *inicio;
    cout << "Imprimindo os números digitados "<<endl;
    while (temp) {
        cout << temp->numero << endl;
        temp = temp->proximo;
    }
    cout << "Fim dos valores numericos" << endl;
}

int main()
{
    adicionar(&inicio,&fim);
    adicionar(&inicio,&fim);
    adicionar(&inicio,&fim);
    adicionar(&inicio,&fim);
    listar(&inicio);
    return 0;
}
